﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Transfer
{
    public class estadoperfilrankingdt
    {
        public int idestadoperfil { get; set; }
        public int nivelraking { get; set; }
        public Nullable<int> puntosactuales { get; set; }

    }


    public class estadoperfilrankingdt2
    {
        public int idestadoperfil { get; set; }
        public int nivelraking { get; set; }
        public Nullable<int> puntosactuales { get; set; }

        public rankingglobaldt2 ranking { get; set; }


    }


    public class estadoperfilrankingdtpuntos
    {
        public Nullable<int> puntos { get; set; }

    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;
using WebApi.Transfer;

namespace WebApi.Controllers
{
    public class RankingController : ApiController
    {


        //VALORACION REPORTE

        [HttpGet]
        [Route("api/Ranking/RegistrarValoracionReporte")]
        public valoracionreporteURLdt RegistrarValoracionReporte(string idvaloracion, string segundotipovaloracion)
        {
            valoracionreporteURLdt obj = new valoracionreporteURLdt()
            {
                idvaloracion = idvaloracion,
                segundotipovaloracion = segundotipovaloracion
            };

            return valoracionreporte.RegistrarValoracionReporte(obj);
        }


        [HttpGet]
        [Route("api/Ranking/ObtenerUrlAleatorio")]
        public reporteURLdt ObtenerUrlAleatorio()
        {
            return valoracionreporte.ObtenerUrlAleatorio();
        }


        [HttpGet]
        [Route("api/Ranking/ActualizarValoracionReporte")]
        public valoracionreporteURLdt ActualizarValoracionReporte(string id,string segundotipovaloracion)
        {
            valoracionreporteURLdt obj = new valoracionreporteURLdt()
            {
                segundotipovaloracion = segundotipovaloracion 
            };
            return valoracionreporte.ActualizarValoracionReporte(id,obj);
        }


        [HttpGet]
        [Route("api/Ranking/ObtenerValoracionReporte")]
        public valoracionreporteURLdt ObtenerValoracionReporte(string id)
        {
            return valoracionreporte.ObtenerValoracionReporte(id);
        }



        //PERFIL RANKING
        [HttpGet]
        [Route("api/Ranking/ListarRankingPuntos")]
        public IEnumerable<estadoperfilrankingdt> ListarRankingPuntos()
        {
            return estadoperfilranking.ListarRankingPuntos();
        }


        [HttpGet]
        [Route("api/Ranking/ListarNivelDesbloqueado")]
        public IEnumerable<estadoperfilrankingdt2> ListarNivelDesbloqueado()
        {
            return estadoperfilranking.ListarNivelDesbloqueado();
        }


        [HttpGet]
        [Route("api/Ranking/ObtenerPerfilRanking")]
        public estadoperfilrankingdt ObtenerPerfilRanking(int id)
        {
            return estadoperfilranking.ObtenerPerfilRanking(id);
        }

        [HttpGet]
        [Route("api/Ranking/ObtenerReportadorRanking")]
        public reportadorrankingdt ObtenerReportadorRanking(string id)
        {
            return valoracionreporte.ObtenerReportadorRanking(id);
        }


        [HttpGet]
        [Route("api/Ranking/ActualizarPuntos")]
        public estadoperfilrankingdt ActualizarPuntos(int id,int puntosactuales)
        {
            estadoperfilrankingdt obj = new estadoperfilrankingdt()
            {
                puntosactuales = puntosactuales
            };
            return valoracionreporte.ActualizarPuntos(id,obj);
        }



        //RANKING GLOBAL
        [HttpGet]
        [Route("api/Ranking/ListarClasififacionNivel")]
        public IEnumerable<rankingglobaldt> ListarClasififacionNivel()
        {
            return rankingglobal.ListarClasififacionNivel();
        }




    }
}

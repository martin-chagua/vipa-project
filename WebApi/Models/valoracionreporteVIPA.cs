﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApi.Transfer;

namespace WebApi.Models
{
    public partial class valoracionreporte
    {

        public static IEnumerable<valoracionreportedt> ListarValoracionReporte()
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            var list = from b in db.valoracionreportes.ToList()
                       select new valoracionreportedt()
                       {
                           idvaloracion = b.idvaloracion,
                           segundotipovaloracion = b.segundotipovaloracion,
                           idreporte = b.idreporte,
                           idreportador = b.idreportador,
                           idtiporeporte = b.idtiporeporte
                       };
            return list;
        }


        public static valoracionreporteURLdt RegistrarValoracionReporte(valoracionreporteURLdt valoracionreporteURLdt)
        {
            vipa_databaseEntities db = new vipa_databaseEntities();

            valoracionreporte valoracionreporte = new valoracionreporte()
            {
                idvaloracion = valoracionreporteURLdt.idvaloracion,
                segundotipovaloracion = valoracionreporteURLdt.segundotipovaloracion
            };

            var reporteURL = valoracionreporte.ObtenerUrlAleatorio(); //Obtener reporte aleatorio.

            valoracionreporte valoracionreporte2 = new valoracionreporte()
            {
                idvaloracion = valoracionreporteURLdt.idvaloracion,
                segundotipovaloracion = valoracionreporteURLdt.segundotipovaloracion,

                idreporte = reporteURL.idreporte,
                idreportador = reporteURL.idreportador,
                idtiporeporte = reporteURL.idtiporeporte
            };

            estadoperfilrankingdt send = new estadoperfilrankingdt();
            send.puntosactuales = PuntosValoracion(valoracionreporte2.segundotipovaloracion); //Obtener puntos segun la valoracion.

            reportadorrankingdt perfil = ObtenerReportadorRanking(valoracionreporte2.idreportador); //Obtener el id del perfilRanking segun el id del reportante.

            var puntosGanados = valoracionreporte.ActualizarPuntos(perfil.idestadoperfil, send); //Actualizar segun el id del reportador.

            db.valoracionreportes.Add(valoracionreporte2);
            db.SaveChanges();
            return ObtenerValoracionReporte(valoracionreporte2.idvaloracion);
        }


        public static reporteURLdt ObtenerUrlAleatorio()
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            var list = from b in db.reportes
                       orderby Guid.NewGuid()
                       select new reporteURLdt
                       {
                           idurlvideo = b.idurlvideo,
                           idreportador = b.idreportador,
                           idreporte = b.idreporte,
                           idtiporeporte = b.idtiporeporte

                       };

            var response = list.FirstOrDefault();
            return response;
        }


        public static estadoperfilrankingdt ActualizarPuntos(int id, estadoperfilrankingdt estadoperfilrankingdt)
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            estadoperfilranking obj = db.estadoperfilrankings.SingleOrDefault(m => m.idestadoperfil == id);
            obj.puntosactuales += estadoperfilrankingdt.puntosactuales;
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
            return ObtenerPerfilRanking(id);
        }


        public static valoracionreporteURLdt ObtenerValoracionReporte(string id)
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            var reporteURL = valoracionreporte.ObtenerUrlAleatorio();
            var obj = db.valoracionreportes.Select(b => new valoracionreporteURLdt()
            {

                idvaloracion = b.idvaloracion,
                segundotipovaloracion = b.segundotipovaloracion,
                reportevideo = reporteURL.idurlvideo,
                idreportador = reporteURL.idreportador,
                idreporte = reporteURL.idreporte,
                idtiporeporte = reporteURL.idtiporeporte,
                puntosganados = new estadoperfilrankingdtpuntos
                {
                    puntos = 50
                }
            }).SingleOrDefault(b => b.idvaloracion == id);

            if (obj == null) obj = new valoracionreporteURLdt() { idvaloracion = "No se encontro la valoracion del reporte."};

            return obj;
        }


        public static valoracionreporteURLdt ActualizarValoracionReporte(string id, valoracionreporteURLdt valoracionreporteURLdt)
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            valoracionreporte obj = db.valoracionreportes.SingleOrDefault(m => m.idvaloracion == id);
            obj.segundotipovaloracion = valoracionreporteURLdt.segundotipovaloracion;
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
            return ObtenerValoracionReporte(id);
        }


        public static estadoperfilrankingdt ObtenerPerfilRanking(int id)
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            var obj = db.estadoperfilrankings.Select(b => new estadoperfilrankingdt()
            {
                idestadoperfil = b.idestadoperfil,
                nivelraking = b.nivelraking,
                puntosactuales = b.puntosactuales
            }).SingleOrDefault(b => b.idestadoperfil == id);

            if (obj == null) obj = new estadoperfilrankingdt() { idestadoperfil = 0 };

            return obj;
        }


        public static reportadorrankingdt ObtenerReportadorRanking(string id)
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            var obj = db.reportadors.Select(b => new reportadorrankingdt()
            {
                idreportador = b.idreportador,
                idestadoperfil = b.idestadoperfil
            }).SingleOrDefault(b => b.idreportador == id);

            if (obj == null) obj = new reportadorrankingdt() { idreportador = "No se encontro el reportador." };

            return obj;
        }


        public static int PuntosValoracion(string valoracion)
        {
            int resultado = 0;

            if (valoracion.Equals("leve")) resultado = 50;

            else if (valoracion.Equals("medio")) resultado = 80;

            else if (valoracion.Equals("moderado")) resultado = 100;

            else if (valoracion.Equals("grave")) resultado = 150;

            else if (valoracion.Equals("extremo")) resultado = 200;

            return resultado;
        }


        public static int ActualizarNivel(int? puntos)
        {
            puntos = 0;
            int nivel=0;

            if (puntos <= 500) nivel = 1;

            else if (puntos > 500 && puntos <= 1000) nivel = 2;

            else if (puntos > 1000 && puntos <= 1500) nivel = 3;

            else if (puntos > 1500 && puntos <= 2000) nivel = 4;

            else if (puntos > 2000) nivel = 5;

            return nivel;
        }


    }
}
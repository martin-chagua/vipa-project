﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApi.Transfer;

namespace WebApi.Models
{
    public partial class estadoperfilranking
    {

        public static IEnumerable<estadoperfilrankingdt> ListarRankingPuntos()
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            var list = from b in db.estadoperfilrankings.OrderByDescending(x => x.puntosactuales)
                       select new estadoperfilrankingdt()
                       {
                           idestadoperfil = b.idestadoperfil,
                           nivelraking = b.nivelraking,
                           puntosactuales = b.puntosactuales
                       };
            return list;
        }


        public static IEnumerable<estadoperfilrankingdt2> ListarNivelDesbloqueado()
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            var list = from b in db.estadoperfilrankings.OrderByDescending(x => x.puntosactuales)
                       select new estadoperfilrankingdt2()
                       {
                           idestadoperfil = b.idestadoperfil,
                           nivelraking = b.nivelraking,
                           puntosactuales = b.puntosactuales,
                           ranking = new rankingglobaldt2{
                               nombrenivelranking = b.rankingglobal.nombrenivelranking,
                               descripcionnivelranking = b.rankingglobal.descripcionnivelranking,
                           }
                       };
            return list;
        }


        public static estadoperfilrankingdt ObtenerPerfilRanking(int id)
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            var obj = db.estadoperfilrankings.Select(b => new estadoperfilrankingdt()
            {
                idestadoperfil = b.idestadoperfil,
                nivelraking = b.nivelraking,
                puntosactuales = b.puntosactuales
            }).SingleOrDefault(b => b.idestadoperfil == id);

            if (obj == null) obj = new estadoperfilrankingdt() { idestadoperfil = 0 };

            return obj;
        }



    }
}
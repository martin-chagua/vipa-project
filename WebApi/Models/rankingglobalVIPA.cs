﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Transfer;

namespace WebApi.Models
{
    public partial class rankingglobal
    {
        public static IEnumerable<rankingglobaldt> ListarClasififacionNivel()
        {
            vipa_databaseEntities db = new vipa_databaseEntities();
            var list = from b in db.rankingglobals.ToList()
                       select new rankingglobaldt()
                       {
                           nivelraking = b.nivelraking,
                           nombrenivelranking = b.nombrenivelranking,
                           descripcionnivelranking = b.descripcionnivelranking,
                           puntosmaximos = b.puntosmaximos
                       };
            return list;
        }

    }
}
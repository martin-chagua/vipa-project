USE [master]
GO
/****** Object:  Database [vipa_database]    Script Date: 13/07/2021 12:17:55 ******/
CREATE DATABASE [vipa_database]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'vipa_database', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\vipa_database.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'vipa_database_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\vipa_database_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [vipa_database] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [vipa_database].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [vipa_database] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [vipa_database] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [vipa_database] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [vipa_database] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [vipa_database] SET ARITHABORT OFF 
GO
ALTER DATABASE [vipa_database] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [vipa_database] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [vipa_database] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [vipa_database] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [vipa_database] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [vipa_database] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [vipa_database] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [vipa_database] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [vipa_database] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [vipa_database] SET  ENABLE_BROKER 
GO
ALTER DATABASE [vipa_database] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [vipa_database] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [vipa_database] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [vipa_database] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [vipa_database] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [vipa_database] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [vipa_database] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [vipa_database] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [vipa_database] SET  MULTI_USER 
GO
ALTER DATABASE [vipa_database] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [vipa_database] SET DB_CHAINING OFF 
GO
ALTER DATABASE [vipa_database] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [vipa_database] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [vipa_database] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [vipa_database] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [vipa_database] SET QUERY_STORE = OFF
GO
USE [vipa_database]
GO
/****** Object:  Table [dbo].[cliente]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cliente](
	[idcliente] [nvarchar](8) NOT NULL,
	[nombrecliente] [nvarchar](60) NULL,
	[nombredistrito] [nvarchar](60) NULL,
	[fechacreacion] [date] NULL,
 CONSTRAINT [PK_cliente_idcliente] PRIMARY KEY CLUSTERED 
(
	[idcliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[consejo]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[consejo](
	[idconsejo] [nvarchar](8) NOT NULL,
	[nombreconsejo] [nvarchar](255) NOT NULL,
	[urlconsejovideo] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_consejos_idconsejo] PRIMARY KEY CLUSTERED 
(
	[idconsejo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cuentaverificada]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cuentaverificada](
	[verificacionperfil] [nvarchar](8) NOT NULL,
	[tokenverificacion] [nvarchar](10) NULL,
	[estadoperfil] [nvarchar](2) NULL,
	[fechacreacion] [datetime] NULL,
 CONSTRAINT [PK_cuentaverificada_verificacionperfil] PRIMARY KEY CLUSTERED 
(
	[verificacionperfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detallereporteusuariocliente]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detallereporteusuariocliente](
	[idreporte] [nvarchar](8) NOT NULL,
	[idreportador] [nvarchar](8) NOT NULL,
	[idtiporeporte] [int] NOT NULL,
	[idcliente] [nvarchar](8) NOT NULL,
 CONSTRAINT [PK_detallereporteusuariocliente_idreporte] PRIMARY KEY CLUSTERED 
(
	[idreporte] ASC,
	[idreportador] ASC,
	[idtiporeporte] ASC,
	[idcliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[distrito]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[distrito](
	[iddistrito] [int] IDENTITY(1,1) NOT NULL,
	[nombredistrito] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[iddistrito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[estadoperfilranking]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[estadoperfilranking](
	[idestadoperfil] [nvarchar](8) NOT NULL,
	[nivelraking] [int] NOT NULL,
	[puntosactuales] [int] NULL,
	[posicionglobalranking] [int] NULL,
 CONSTRAINT [PK_estadoperfilranking_idestadoperfil] PRIMARY KEY CLUSTERED 
(
	[idestadoperfil] ASC,
	[nivelraking] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[notificacion]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[notificacion](
	[idnotificacion] [int] IDENTITY(1,1) NOT NULL,
	[nombrenotificacion] [nvarchar](60) NULL,
	[descripcionnotificacion] [nvarchar](255) NULL,
	[urlincidente] [nvarchar](255) NULL,
	[repeticionnotificacion] [nvarchar](2) NULL,
	[fechadespliegue] [datetime2](0) NULL,
 CONSTRAINT [PK_notificaciones_idnotificacion] PRIMARY KEY CLUSTERED 
(
	[idnotificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[preguntafrecuente]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[preguntafrecuente](
	[numeropregunta] [int] IDENTITY(1,1) NOT NULL,
	[descripcionpregunta] [nvarchar](1000) NULL,
	[descripcionrespuesta] [nvarchar](1000) NULL,
 CONSTRAINT [PK_preguntasfrecuentes_numeropregunta] PRIMARY KEY CLUSTERED 
(
	[numeropregunta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rankingglobal]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rankingglobal](
	[nivelraking] [int] IDENTITY(1,1) NOT NULL,
	[nombrenivelranking] [nvarchar](30) NULL,
	[descripcionnivelranking] [nvarchar](255) NULL,
	[puntosmaximos] [int] NULL,
 CONSTRAINT [PK_rankingglobal_nivelraking] PRIMARY KEY CLUSTERED 
(
	[nivelraking] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reportador]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reportador](
	[idreportador] [nvarchar](8) NOT NULL,
	[aliasusuario] [nvarchar](60) NULL,
	[perfilvisible] [nvarchar](2) NULL,
	[nombrereportador] [nvarchar](60) NULL,
	[apellidosreportador] [nvarchar](60) NULL,
	[correoreportador] [nvarchar](60) NULL,
	[contraseñareportador] [nvarchar](255) NULL,
	[idestadoperfil] [nvarchar](8) NOT NULL,
	[verificacionperfil] [nvarchar](8) NOT NULL,
 CONSTRAINT [PK_reportador] PRIMARY KEY CLUSTERED 
(
	[idreportador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reporte]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reporte](
	[idreporte] [nvarchar](8) NOT NULL,
	[idreportador] [nvarchar](8) NOT NULL,
	[idtiporeporte] [int] NOT NULL,
	[idurlvideo] [text] NULL,
	[idurlgps] [text] NULL,
	[nombredistrito] [nvarchar](60) NULL,
	[idplacavehiculo] [nvarchar](7) NULL,
	[descripcionreporte] [nvarchar](255) NULL,
	[estadoreporte] [nchar](10) NULL,
	[fechacreacion] [datetime] NULL,
 CONSTRAINT [PK_reporte_idreporte] PRIMARY KEY CLUSTERED 
(
	[idreporte] ASC,
	[idreportador] ASC,
	[idtiporeporte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[terminocondicion]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[terminocondicion](
	[reglaid] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nchar](255) NULL,
	[descripcionregla] [text] NULL,
 CONSTRAINT [PK_terminoscondiciones_reglaid] PRIMARY KEY CLUSTERED 
(
	[reglaid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tiporeporte]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tiporeporte](
	[idtiporeporte] [int] IDENTITY(1,1) NOT NULL,
	[reportenombre] [nvarchar](60) NULL,
	[reporteindicador] [nvarchar](2) NULL,
	[idreportepadre] [int] NOT NULL,
 CONSTRAINT [PK_tiporeporte_idtiporeporte] PRIMARY KEY CLUSTERED 
(
	[idtiporeporte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuarioscliente]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuarioscliente](
	[idusuariocliente] [nvarchar](8) NOT NULL,
	[idcliente] [nvarchar](8) NOT NULL,
	[nombreusuariocliente] [nvarchar](60) NULL,
	[correousuariocliente] [nvarchar](60) NULL,
	[contraseñausuariocliente] [nvarchar](10) NULL,
 CONSTRAINT [PK_usuarioscliente_idusuariocliente] PRIMARY KEY CLUSTERED 
(
	[idusuariocliente] ASC,
	[idcliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[valoracionreporte]    Script Date: 13/07/2021 12:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[valoracionreporte](
	[idvaloracion] [nvarchar](8) NOT NULL,
	[segundotipovaloracion] [nvarchar](8) NULL,
	[idreporte] [nvarchar](8) NOT NULL,
	[idreportador] [nvarchar](8) NOT NULL,
	[idtiporeporte] [int] NOT NULL,
 CONSTRAINT [PK_valoracionreporte_idvaloracion] PRIMARY KEY CLUSTERED 
(
	[idvaloracion] ASC,
	[idreporte] ASC,
	[idreportador] ASC,
	[idtiporeporte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[cliente] ([idcliente], [nombrecliente], [nombredistrito], [fechacreacion]) VALUES (N'CE000001', N'Municipalidad de Lima', N'Lima', CAST(N'2021-07-10' AS Date))
INSERT [dbo].[cliente] ([idcliente], [nombrecliente], [nombredistrito], [fechacreacion]) VALUES (N'CE000002', N'Municipalidad de La Victoria', N'La Victoria', CAST(N'2021-07-10' AS Date))
INSERT [dbo].[cliente] ([idcliente], [nombrecliente], [nombredistrito], [fechacreacion]) VALUES (N'RE123', N'JHONATAN', N'lima', CAST(N'2021-12-01' AS Date))
INSERT [dbo].[cliente] ([idcliente], [nombrecliente], [nombredistrito], [fechacreacion]) VALUES (N'RE125', N'JUAN', N'surco', CAST(N'2021-12-03' AS Date))
GO
INSERT [dbo].[consejo] ([idconsejo], [nombreconsejo], [urlconsejovideo]) VALUES (N'C001', N'Como grabar correctamente', N'https://www.youtube.com/watch?v=nhL4IWUFSzU')
INSERT [dbo].[consejo] ([idconsejo], [nombreconsejo], [urlconsejovideo]) VALUES (N'C002', N'Como registrarme', N'https://www.youtube.com/watch?v=nhL4IWUFSzU')
INSERT [dbo].[consejo] ([idconsejo], [nombreconsejo], [urlconsejovideo]) VALUES (N'C003', N'Como seguir tu reporte', N'https://www.youtube.com/watch?v=nhL4IWUFSzU')
INSERT [dbo].[consejo] ([idconsejo], [nombreconsejo], [urlconsejovideo]) VALUES (N'C004', N'Como utilizar la aplicación VIPA', N'https://www.youtube.com/watch?v=nhL4IWUFSzU')
INSERT [dbo].[consejo] ([idconsejo], [nombreconsejo], [urlconsejovideo]) VALUES (N'C005', N'Como obtener puntos', N'https://www.youtube.com/watch?v=nhL4IWUFSzU')
INSERT [dbo].[consejo] ([idconsejo], [nombreconsejo], [urlconsejovideo]) VALUES (N'C006', N'Tips VIPA', N'https://www.youtube.com/watch?v=nhL4IWUFSzU')
GO
INSERT [dbo].[cuentaverificada] ([verificacionperfil], [tokenverificacion], [estadoperfil], [fechacreacion]) VALUES (N'VE000001', N'XDXD123456', N'1', CAST(N'2021-06-26T23:33:35.723' AS DateTime))
INSERT [dbo].[cuentaverificada] ([verificacionperfil], [tokenverificacion], [estadoperfil], [fechacreacion]) VALUES (N'VE000002', N'FFFF123457', N'1', CAST(N'2021-06-26T23:33:35.723' AS DateTime))
INSERT [dbo].[cuentaverificada] ([verificacionperfil], [tokenverificacion], [estadoperfil], [fechacreacion]) VALUES (N'VE000003', N'FFFF123400', N'1', CAST(N'2021-07-10T13:04:43.270' AS DateTime))
INSERT [dbo].[cuentaverificada] ([verificacionperfil], [tokenverificacion], [estadoperfil], [fechacreacion]) VALUES (N'VE000004', N'FFFF003457', N'1', CAST(N'2021-07-10T13:04:43.270' AS DateTime))
INSERT [dbo].[cuentaverificada] ([verificacionperfil], [tokenverificacion], [estadoperfil], [fechacreacion]) VALUES (N'VE000005', N'FFFF123400', N'1', CAST(N'2021-07-10T13:04:43.270' AS DateTime))
INSERT [dbo].[cuentaverificada] ([verificacionperfil], [tokenverificacion], [estadoperfil], [fechacreacion]) VALUES (N'VE000006', N'FFFF003457', N'1', CAST(N'2021-07-10T13:04:43.270' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[distrito] ON 

INSERT [dbo].[distrito] ([iddistrito], [nombredistrito]) VALUES (1, N'San Juan de Miraflores')
INSERT [dbo].[distrito] ([iddistrito], [nombredistrito]) VALUES (2, N'Santiago de Surco')
INSERT [dbo].[distrito] ([iddistrito], [nombredistrito]) VALUES (3, N'San Juan de Lurigancho')
INSERT [dbo].[distrito] ([iddistrito], [nombredistrito]) VALUES (4, N'Lima')
SET IDENTITY_INSERT [dbo].[distrito] OFF
GO
INSERT [dbo].[estadoperfilranking] ([idestadoperfil], [nivelraking], [puntosactuales], [posicionglobalranking]) VALUES (N'E000001', 1, 50, 5)
INSERT [dbo].[estadoperfilranking] ([idestadoperfil], [nivelraking], [puntosactuales], [posicionglobalranking]) VALUES (N'E000002', 2, 1200, 4)
INSERT [dbo].[estadoperfilranking] ([idestadoperfil], [nivelraking], [puntosactuales], [posicionglobalranking]) VALUES (N'E000003', 2, 1400, 0)
INSERT [dbo].[estadoperfilranking] ([idestadoperfil], [nivelraking], [puntosactuales], [posicionglobalranking]) VALUES (N'E000004', 3, 1800, 0)
INSERT [dbo].[estadoperfilranking] ([idestadoperfil], [nivelraking], [puntosactuales], [posicionglobalranking]) VALUES (N'E000005', 4, 2200, 0)
INSERT [dbo].[estadoperfilranking] ([idestadoperfil], [nivelraking], [puntosactuales], [posicionglobalranking]) VALUES (N'E000006', 4, 2400, 0)
GO
SET IDENTITY_INSERT [dbo].[notificacion] ON 

INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (1, N'Actualizacion de aplicacion', N'Nueva version 1.18.8 disponible', N'https://play.google.com/store/apps/details?id=pe.aplicativos.videopapeleta&hl=es_PE&gl=US', N'1', CAST(N'2021-06-26T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (2, N'Actos Vandálicos', N'Cámaras de vigilancia captan impactantes accidentes de tránsito en el Centro de Lima', N'https://www.youtube.com/watch?v=VfMif8sS4yQ', N'0', CAST(N'2021-06-24T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (3, N'Accidente de trafico', N'Aparatoso accidente de tránsito en Iquitos', N'https://www.youtube.com/watch?v=t2UNzEJXAbQ', N'0', CAST(N'2021-06-22T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (4, N'Robos en la Victoria', N'La Victoria: reportan constantes robos en el cruce de las avenidas San Pablo y México', N'https://www.youtube.com/watch?v=n9wyTxRIG6k', N'1', CAST(N'2021-06-25T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (5, N'Actualizacion de aplicacion', N'Nueva version 1.17.8 disponible', N'https://play.google.com/store/apps/details?id=pe.aplicativos.videopapeleta&hl=es_PE&gl=US', N'1', CAST(N'2021-02-10T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (6, N'Actualizacion de aplicacion', N'Nueva version 1.15.8 disponible', N'https://play.google.com/store/apps/details?id=pe.aplicativos.videopapeleta&hl=es_PE&gl=US', N'1', CAST(N'2020-09-16T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (7, N'Actualizacion de aplicacion', N'Nueva version 1.14.8 disponible', N'https://play.google.com/store/apps/details?id=pe.aplicativos.videopapeleta&hl=es_PE&gl=US', N'0', CAST(N'2019-06-18T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (8, N'Estado de Reporte', N'Tu reporte de ID REPO0010 a actualizado su estado', N'https://www.youtube.com/watch?v=VfMif8sS4yQ', N'0', CAST(N'2021-06-24T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (9, N'Estado de Reporte', N'Tu reporte de ID RE000011 a actualizado su estado', N'https://www.youtube.com/watch?v=VfMif8sS4yQ', N'0', CAST(N'2021-06-09T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (10, N'Estado de Reporte', N'Tu reporte de ID RE000012', N'https://www.youtube.com/watch?v=VfMif8sS4yQ', N'1', CAST(N'2021-05-20T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[notificacion] ([idnotificacion], [nombrenotificacion], [descripcionnotificacion], [urlincidente], [repeticionnotificacion], [fechadespliegue]) VALUES (11, N'Estado de Reporte', N'Tu reporte de ID RE000014', N'https://www.youtube.com/watch?v=VfMif8sS4yQ', N'1', CAST(N'2021-05-05T00:00:00.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[notificacion] OFF
GO
SET IDENTITY_INSERT [dbo].[preguntafrecuente] ON 

INSERT [dbo].[preguntafrecuente] ([numeropregunta], [descripcionpregunta], [descripcionrespuesta]) VALUES (1, N'¿Quiénes somos?', N'VIPA (antes VideoPaleta) es una inciativa privada que busca generar un impacto positivo en la educación vial, seguridad y respeto por el prójimo en el país. Pertenece al laboratorio creativo Aplicativos SAC y ha sido totalmente financiada por sus fundadores.')
INSERT [dbo].[preguntafrecuente] ([numeropregunta], [descripcionpregunta], [descripcionrespuesta]) VALUES (2, N'¿Con quiénes trabajamos?', N'Actualmente trabajamos con la Municipalidad de Lima y Piura y se atienden videos de todos los distritos de Lima. Poco a poco se están sumando más Municipalidad. Si aún no trabajamos con la Municipalidad del distrito donde vives o trabajas, de igual manera tus videos sirven para generar contenido de educación cívica y vial. Tambien sirven como sustento pra lograr que la Municipalidad comience a usar VIPA.  Las Municipalidades atienden los videos según sus ordenanzas y/o reglamentos, y ellas son las responsables de iniciar los procesos sancionadores.')
INSERT [dbo].[preguntafrecuente] ([numeropregunta], [descripcionpregunta], [descripcionrespuesta]) VALUES (3, N'¿En qué ciudades puedo reportar?', N'Se pueden subir videos en todo el país, y esperamos pronto trabajar con Municipalidades en todas las provincias pra que sean atendidos. De igual manera, si no se atienden aún en tu ciudad, tus videos servirán para contenido de educación cívica y vial.')
INSERT [dbo].[preguntafrecuente] ([numeropregunta], [descripcionpregunta], [descripcionrespuesta]) VALUES (4, N'¿Qué tipos de infracciones se pueden reportar?', N'Constantemente agregamos nuevas categorías para reportar. Te invitamos a descargar el app para que puedas ver la lista actualizada.')
INSERT [dbo].[preguntafrecuente] ([numeropregunta], [descripcionpregunta], [descripcionrespuesta]) VALUES (5, N'¿Que infracciones pueden mltar las Municipalidades y cuáles sonlos montos de las multas?', N'Las Municipalidades Distritales imponen multas administrativas según sus ordenanzas municipales. Cada Municipalidad también define internamente el monto de estas multas.')
SET IDENTITY_INSERT [dbo].[preguntafrecuente] OFF
GO
SET IDENTITY_INSERT [dbo].[rankingglobal] ON 

INSERT [dbo].[rankingglobal] ([nivelraking], [nombrenivelranking], [descripcionnivelranking], [puntosmaximos]) VALUES (1, N'Entusiasta', N'Cuando usas VIPA por primera vez', 500)
INSERT [dbo].[rankingglobal] ([nivelraking], [nombrenivelranking], [descripcionnivelranking], [puntosmaximos]) VALUES (2, N'Emergente', N'Cuando logras pasar al promedio de usuarios', 1000)
INSERT [dbo].[rankingglobal] ([nivelraking], [nombrenivelranking], [descripcionnivelranking], [puntosmaximos]) VALUES (3, N'Amateur', N'Cuando generas reportes validados en tu distrito', 1500)
INSERT [dbo].[rankingglobal] ([nivelraking], [nombrenivelranking], [descripcionnivelranking], [puntosmaximos]) VALUES (4, N'Profesional', N'Cuando ya generaste reportes que han contribuido a la ciudad', 2000)
INSERT [dbo].[rankingglobal] ([nivelraking], [nombrenivelranking], [descripcionnivelranking], [puntosmaximos]) VALUES (5, N'Guardian', N'Cuando superas al promedio de los usuarios profesionales', 3000)
SET IDENTITY_INSERT [dbo].[rankingglobal] OFF
GO
INSERT [dbo].[reportador] ([idreportador], [aliasusuario], [perfilvisible], [nombrereportador], [apellidosreportador], [correoreportador], [contraseñareportador], [idestadoperfil], [verificacionperfil]) VALUES (N'RE000001', N'@DanielR', N'1', N'Daniel', N'Rodriguez Escalante', N'Daniel12369@hotmail.com', N'123', N'E000001', N'VE000001')
INSERT [dbo].[reportador] ([idreportador], [aliasusuario], [perfilvisible], [nombrereportador], [apellidosreportador], [correoreportador], [contraseñareportador], [idestadoperfil], [verificacionperfil]) VALUES (N'RE000002', N'Claudia@234', N'1', N'Claduia', N'Quispe Baldeon', N'claudiaqb@outlook.com', N'123', N'E000002', N'VE000002')
INSERT [dbo].[reportador] ([idreportador], [aliasusuario], [perfilvisible], [nombrereportador], [apellidosreportador], [correoreportador], [contraseñareportador], [idestadoperfil], [verificacionperfil]) VALUES (N'RE000003', N'Manuel123', N'1', N'Manuel', N'Agurto Sanchez', N'msanchez@outlook.com', N'123', N'E000003', N'VE000003')
INSERT [dbo].[reportador] ([idreportador], [aliasusuario], [perfilvisible], [nombrereportador], [apellidosreportador], [correoreportador], [contraseñareportador], [idestadoperfil], [verificacionperfil]) VALUES (N'RE000004', N'Carlos@234', N'1', N'Carlos', N'Estrada Manrique', N'cestrada@outlook.com', N'123', N'E000004', N'VE000004')
INSERT [dbo].[reportador] ([idreportador], [aliasusuario], [perfilvisible], [nombrereportador], [apellidosreportador], [correoreportador], [contraseñareportador], [idestadoperfil], [verificacionperfil]) VALUES (N'RE000005', N'Esteban64', N'1', N'Esteban', N'Medina Ocmin', N'emedina@outlook.com', N'123', N'E000005', N'VE000005')
INSERT [dbo].[reportador] ([idreportador], [aliasusuario], [perfilvisible], [nombrereportador], [apellidosreportador], [correoreportador], [contraseñareportador], [idestadoperfil], [verificacionperfil]) VALUES (N'RE000006', N'Yolanda@234', N'1', N'Yolanda', N'Veliz Montes', N'yveliz@outlook.com', N'123', N'E000006', N'VE000006')
GO
INSERT [dbo].[reporte] ([idreporte], [idreportador], [idtiporeporte], [idurlvideo], [idurlgps], [nombredistrito], [idplacavehiculo], [descripcionreporte], [estadoreporte], [fechacreacion]) VALUES (N'IN000006', N'RE000001', 1, N'http://localhost:54180/vids/v1.mp4', N'https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d5060.151543022814!2d-77.03916854388561!3d-12.049696379554197!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2spe!4v1626193683915!5m2!1ses!2spe', N'Lima', N'ABC-123', N'Descripción del reporte 1', N'Enviado   ', CAST(N'2021-07-13T12:07:05.580' AS DateTime))
INSERT [dbo].[reporte] ([idreporte], [idreportador], [idtiporeporte], [idurlvideo], [idurlgps], [nombredistrito], [idplacavehiculo], [descripcionreporte], [estadoreporte], [fechacreacion]) VALUES (N'IN000007', N'RE000001', 2, N'http://localhost:54180/vids/v2.mp4', N'https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d5059.632157714196!2d-77.09071272291159!3d-12.077215673478348!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2spe!4v1626194075169!5m2!1ses!2spe', N'San Miguel', N'ABC-456', N'Descripción del reporte 2', N'Enviado   ', CAST(N'2021-07-13T12:07:05.583' AS DateTime))
INSERT [dbo].[reporte] ([idreporte], [idreportador], [idtiporeporte], [idurlvideo], [idurlgps], [nombredistrito], [idplacavehiculo], [descripcionreporte], [estadoreporte], [fechacreacion]) VALUES (N'IN000008', N'RE000001', 3, N'http://localhost:54180/vids/v3.mp4', N'https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1788.2418582282323!2d-76.98045291530455!3d-12.16793100077143!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2spe!4v1626194151962!5m2!1ses!2spe', N'San Juan de Miraflores', N'ABC-789', N'Descripción del reporte 3', N'Enviado   ', CAST(N'2021-07-13T12:07:05.583' AS DateTime))
INSERT [dbo].[reporte] ([idreporte], [idreportador], [idtiporeporte], [idurlvideo], [idurlgps], [nombredistrito], [idplacavehiculo], [descripcionreporte], [estadoreporte], [fechacreacion]) VALUES (N'IN000009', N'RE000006', 4, N'http://localhost:54180/vids/v4.mp4', N'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6561.030962650963!2d-77.0489261314822!3d-12.09761022196931!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c842bd2342fb%3A0x7e3183f45f461207!2sSan%20Isidro!5e0!3m2!1ses!2spe!4v1626194206837!5m2!1ses!2spe', N'San Isidro', N'XYZ-123', N'Descripción del reporte 4', N'Enviado   ', CAST(N'2021-07-13T12:07:05.583' AS DateTime))
INSERT [dbo].[reporte] ([idreporte], [idreportador], [idtiporeporte], [idurlvideo], [idurlgps], [nombredistrito], [idplacavehiculo], [descripcionreporte], [estadoreporte], [fechacreacion]) VALUES (N'IN000010', N'RE000003', 1, N'http://localhost:54180/vids/v5.mp4', N'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5060.180363839738!2d-77.04487516273355!3d-12.048167513782635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c842bd2342fb%3A0x7e3183f45f461207!2sSan%20Isidro!5e0!3m2!1ses!2spe!4v1626195085851!5m2!1ses!2spe', N'Lima', N'ABC-123', N'Descripción del reporte 1', N'Enviado   ', CAST(N'2021-07-13T12:07:05.583' AS DateTime))
INSERT [dbo].[reporte] ([idreporte], [idreportador], [idtiporeporte], [idurlvideo], [idurlgps], [nombredistrito], [idplacavehiculo], [descripcionreporte], [estadoreporte], [fechacreacion]) VALUES (N'IN000011', N'RE000004', 2, N'http://localhost:54180/vids/v6.mp4', N'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3008.472108058673!2d-77.10139976028377!3d-12.07749805619767!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c964223bb347%3A0x2013e50c7a7d23d4!2sSan%20Miguel!5e0!3m2!1ses!2spe!4v1626195163566!5m2!1ses!2spe', N'San Miguel', N'ABC-456', N'Descripción del reporte 2', N'Enviado   ', CAST(N'2021-07-13T12:07:05.583' AS DateTime))
INSERT [dbo].[reporte] ([idreporte], [idreportador], [idtiporeporte], [idurlvideo], [idurlgps], [nombredistrito], [idplacavehiculo], [descripcionreporte], [estadoreporte], [fechacreacion]) VALUES (N'IN000012', N'RE000005', 3, N'http://localhost:54180/vids/v7.mp4', N'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3576.6935295041803!2d-76.9784578490748!3d-12.152332589527953!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105b8f71b53ab49%3A0xdd8cbd3088d235f0!2sSan%20Juan%20de%20Miraflores!5e0!3m2!1ses!2spe!4v1626195219902!5m2!1ses!2spe', N'San Juan de Miraflores', N'ABC-789', N'Descripción del reporte 3', N'Enviado   ', CAST(N'2021-07-13T12:07:05.583' AS DateTime))
INSERT [dbo].[reporte] ([idreporte], [idreportador], [idtiporeporte], [idurlvideo], [idurlgps], [nombredistrito], [idplacavehiculo], [descripcionreporte], [estadoreporte], [fechacreacion]) VALUES (N'IN000013', N'RE000006', 4, N'http://localhost:54180/vids/v8.mp4', N'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1788.7326759279435!2d-77.05568229013399!3d-12.094781463892053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c842bd2342fb%3A0x7e3183f45f461207!2sSan%20Isidro!5e0!3m2!1ses!2spe!4v1626195254574!5m2!1ses!2spe', N'San Isidro', N'XYZ-123', N'Descripción del reporte 4', N'Enviado   ', CAST(N'2021-07-13T12:07:05.583' AS DateTime))
INSERT [dbo].[reporte] ([idreporte], [idreportador], [idtiporeporte], [idurlvideo], [idurlgps], [nombredistrito], [idplacavehiculo], [descripcionreporte], [estadoreporte], [fechacreacion]) VALUES (N'IN000014', N'RE000006', 4, N'http://localhost:54180/vids/v9.mp4', N'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1063.667355085589!2d-77.09616205808756!3d-12.0745167927369!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c964223bb347%3A0x2013e50c7a7d23d4!2sSan%20Miguel!5e0!3m2!1ses!2spe!4v1626195288211!5m2!1ses!2spe', N'San Miguel', N'XYZ-456', N'Descripción del reporte 5', N'Enviado   ', CAST(N'2021-07-13T12:07:05.583' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[terminocondicion] ON 

INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (1, N'Información al REPORTANTE                                                                                                                                                                                                                                      ', N'El acceso, registro y/o uso de la app VIPA y del sitio web es totalmente voluntario y gratuito, y atribuye a quien lo realiza la condición de REPORTANTE. Todo REPORTANTE acepta, desde el mismo momento en el que accede, sin ningún tipo de reserva, el contenido de las presentes "Políticas y Condiciones” así como, en su caso, las Condiciones Particulares que puedan complementarlas, sustituirlas o modificarlas en algún sentido en relación con los servicios y contenidos de la app y de la web. En consecuencia, el REPORTANTE deberá leer detenidamente dichos documentos antes del acceso y de utilización de cualquier servicio de la app y web bajo su entera responsabilidad. El REPORTANTE puede acceder, imprimir, descargar y guardar las Políticas y Condiciones en todo momento. Estas Condiciones estarán permanentemente accesibles en la app VIPA y en el sitio web a través del enlace “Términos, Condiciones y Políticas de Privacidad”. VIPA se reserva la posibilidad de modificar, sin previo aviso, el contenido de las Condiciones Generales de Uso y la Política de Privacidad de manera que recomienda al REPORTANTE leer dichas Condiciones y Políticas cada vez que éste acceda a y utilice la app y la página web. Asimismo, VIPA se reserva la posibilidad de modificar sin previo aviso, el diseño, presentación y/o configuración de la app y web, así como algunos o todos los servicios, y añadir servicios nuevos. En cualquier caso, VIPA se reserva el derecho de que en cualquier momento y sin necesidad de previo aviso podrá denegar el acceso a este sitio a aquellos REPORTANTES que incumplan estas condiciones generales o de las particulares que les sean de aplicación.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (2, N'1. DESCRIPCIÓN DEL SERVICIO                                                                                                                                                                                                                                    ', N'VIPA es una app que permite a los usuarios capturar y reportar incidentes de interés ciudadano, enfocándose principalmente en aquellos que suceden en la vía pública, como por ejemplo faltas de vehículos motorizados, a través de sus propios dispositivos móviles. Los incidentes son registrados por medio de videos, capturando la ubicación (con previa aprobación del REPORTANTE), fecha y hora, sin que puedan ser alteradas o editadas posteriormente. De esta manera se garantiza veracidad de la evidencia fílmica, que será enviada a: 1) las autoridades competentes con quienes tengamos relaciones, para que tomen las acciones correspondientes, en medida que el material cumpla con los requerimientos de dichas autoridades (si hace referencia a una infracción o a un problema de infraestructura), de manera anónima; 2) la comunidad de usuarios de VIPA con fines de interés público . De esta manera, por medio de la colaboración ciudadana y el control social, VIPA contribuye con la mejora de la ciudad, la educación y el cumplimiento de normas como las de seguridad ciudadana y tránsito, logrando que la sociedad se autorregule, mejorando la convivencia de todos. VIPA provee a los REPORTANTES el acceso a sus servicios por medio de la app y su sitio web. Mediante el acceso al servicio de VIPA, el REPORTANTE conviene y acepta que VIPA no será responsable por el retraso, borrado, entrega equivocada o falla al guardar o enviar cualquier contenido digital disponible a través de este sitio, en virtud de que el servicio que presta VIPA y su eficacia dependen de la disponibilidad del mismo. VIPA tendrá la facultad de eliminar de la app y sito web, sin previo aviso, contenidos que contravengan las Políticas y Condiciones, la normativa legal vigente o las buenas costumbres. El REPORTANTE reconoce que técnicamente no es posible lograr disponibilidad ininterrumpida de la app VIPA y del sitio web. Sin embargo, VIPA se esforzará por mantener disponible el servicio de forma continua. Especialmente por razones de mantenimiento, seguridad o capacidad, así como a causa de acontecimientos sobre los que no tiene control VIPA, pueden producirse breves anomalías o la suspensión temporal de los servicios.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (3, N'2. ACEPTACIÓN DEL REPORTANTE                                                                                                                                                                                                                                   ', N'Al utilizar y/o visitar cualquier servicio o página que forme parte de VIPA, usted ("REPORTANTE") acepta los términos y condiciones; las Políticas de Privacidad y Políticas de Propiedad Intelectual de VIPA. Si el REPORTANTE no acepta alguno de estos términos, el REPORTANTE no está autorizado a utilizar VIPA. Además, VIPA podría, a discreción, modificar o revisar los Términos de Servicio y las políticas de VIPA en cualquier momento, y el REPORTANTE deberá aceptar dichas modificaciones o revisiones si desea continuar utilizando cualquier servicio o página que forme parte de VIPA. VIPA recomienda al REPORTANTE revisar periódicamente la versión vigente de los Términos de Servicio. Debe entenderse que nada en los Términos de Servicio confiere derechos o beneficios a terceros. El REPORTANTE se compromete a hacer un uso diligente del mismo y de los servicios accesibles de la app y del sitio web, con total sujeción a la Ley, a las buenas costumbres y a los presentes Términos y Condiciones Generales de Uso, así como mantener el debido respeto a los demás REPORTANTES.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (4, N'3. SUSCRIPCIÓN Y REGISTRO DE CUENTA DE REPORTANTE EN VIPA                                                                                                                                                                                                      ', N'Para acceder a algunas características de VIPA, el REPORTANTE debe crear una cuenta en la app o web de VIPA. El REPORTANTE no está autorizado a usar la cuenta de otra persona sin su permiso expreso. Al crear una cuenta en VIPA, el REPORTANTE debe brindar información precisa y completa. El REPORTANTE es el único responsable de toda actividad que ocurra en la cuenta del REPORTANTE, y es responsable de mantener la seguridad de su propia contraseña de ingreso. Si el REPORTANTE se percata de cualquier violación de seguridad o uso no autorizado de su cuenta, el REPORTANTE debe informar a VIPA inmediatamente. Si bien VIPA no será responsable por el mal uso o las pérdidas del REPORTANTE a causa del uso no autorizado de la cuenta del mismo, el REPORTANTE sí será responsable por las pérdidas de VIPA u otros debido a dicho uso no autorizado.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (5, N'4. CONDUCTA Y CONTENIDO DE LOS REPORTANTES                                                                                                                                                                                                                     ', N'Como usuario de una cuenta en VIPA, se puede permitir al REPORTANTE proporcionar contenido, incluido videos y contenido textual ("Comentarios del REPORTANTE"). Todo contenido proporcionado por el REPORTANTE está sujeto a los términos de Servicio, las Políticas de Propiedad Intelectual de VIPA, y toda la Normativa Legal vigente aplicable. El REPORTANTE es el único responsable de todo el contenido proporcionado a VIPA, incluyendo, sin limitación, videos y textos ("el contenido"). Asimismo, el REPORTANTE permite la inclusión y uso del contenido proporcionado de todas las maneras contempladas por VIPA y los Términos de Servicio. A menos que se indique lo contrario en estos Términos de Servicio o la Política de Propiedad Intelectual, el REPORTANTE retiene la autoría, y derechos de propiedad intelectual que le correspondan bajo las leyes peruanas, sobre el contenido que proporciona. Sin embargo, al proporcionar contenido a VIPA, el REPORTANTE le otorga a VIPA una autorización de uso que compone una licencia mundial, no exclusiva, libre de regalías y transferible para usar, reproducir, distribuir, preparar trabajos entregables, divulgar y desarrollar dicho contenido del REPORTANTE en conexión con la página web y los negocios de VIPA, incluido, sin limitación, para promocionar y redistribuir parte o todo lo contenido en VIPA en cualquier formato de comunicación y a cualquier canal, actual, o por descubrirse en el futuro. El REPORTANTE acepta que no proporcionará material sobre el que no tenga derechos de autor, que esté protegido por secreto comercial, o de otra forma esté sujeto a los derechos de propiedad de terceras partes, incluido derechos de privacidad y de publicidad; a menos que el REPORTANTE sea el propietario de dichos derechos o cuente con completo permiso del propietario para subir material y otorgar las licencias descritas antes aquí. El REPORTANTE acepta que VIPA tiene el derecho de rechazar la aceptación, de limitar el acceso a, de remover, y/o rechazar la publicación de cualquier contenido, el cual sea considerado por VIPA como amoral, escandaloso, ofensivo, de mal gusto, u otros a su entera discreción. Todas las determinaciones en cuanto al contenido del REPORTANTE, si es amoral, escandaloso, ofensivo, o de mal gusto, están sujetas a única y final discreción de VIPA. El REPORTANTE está obligado a aceptar cualquier decisión de VIPA respecto a este tema. El REPORTANTE acepta no comprometerse en el uso, copia, o distribución de cualquier contenido de la página web de VIPA, ya sea sólo de la manera expresa permitida aquí, incluido todo uso, copia, o distribución del contenido del REPORTANTE de terceras partes obtenido a través de VIPA para cualquier propósito comercial. El REPORTANTE entiende que puede estar expuesto a contenidos de diversas fuentes, y que VIPA no es responsable por la precisión, utilidad, seguridad, o derechos de propiedad intelectual o derechos relacionados al contenido proporcionado por el REPORTANTE. Asimismo, el REPORTANTE entiende y reconoce que puede estar expuesto a contenido impreciso, ofensivo, indecente, o censurable y el REPORTANTE acepta renunciar, y por la presente renuncia, a cualquier derecho legal o imparcial o recursos legales que el REPORTANTE tenga o pueda tener contra VIPA con respecto a lo antes mencionado, y acepta indemnizar y librar a VIPA, sus propietarios, operadores, afiliados y/o licenciatarios, de toda responsabilidad permitida por ley de todos los asuntos relacionados al uso de la app y este sitio. Información facilitada por los REPORTANTES. Los REPORTANTES que ingresen información de personas fallecidas en VIPA, se declaran familiares directos de las personas fallecidas o como personas autorizadas por los familiares de éstas para comunicar públicamente a través de VIPA toda la información facilitada. VIPA no permite la inserción de contenidos que deterioren la calidad del servicio. Está prohibida la inserción de contenidos: Que sean presuntamente ilícitos por la normativa nacional, comunitaria o internacional o que realicen actividades presuntamente ilícitas o contravengan los principios de la buena fe. Que no reúnan los parámetros de calidad establecidos en VIPA. Que atenten contra los derechos fundamentales de las personas y familiares de los fallecidos, puedan buscar la debilidad del REPORTANTE, falten a la cortesía en la red, molesten o puedan generar opiniones negativas en nuestros REPORTANTES o terceros. En concreto, y a modo enunciativo pero no limitativo: cualesquiera de los derechos legales de terceros; aquellos que favorezcan o potencien la creación, mantenimiento y fomento de negocios relacionados con la pornografía, material obsceno o gestión de contactos eróticos; aquellos que estén relacionados con la videncia, tarot, ciencias ocultas o cualquier otro contenido relacionado; y en general cualesquiera contenidos que VIPA considere no apropiados para los REPORTANTES y, en especial, para los menores de edad. En general, aquellos contenidos que contravengan los principios de legalidad, honradez, responsabilidad, protección de la dignidad humana, protección de menores, protección del orden público, la protección de la vida privada, la protección del consumidor y los derechos de propiedad intelectual e industrial. Asimismo, VIPA se reserva el derecho de retirar aquellos contenidos que se consideren no apropiados a las características y finalidades de VIPA. Asimismo, en virtud que no puede controlar todos y cada uno de los contenidos publicados, no puede asumir la responsabilidad sobre los contenidos. Si usted advierte algún contenido inapropiado en el sitio web, por favor contáctese con nosotros a través de nuestro servicio de Atención al REPORTANTE: contacto@aplicativos.pe. Cualquier REPORTANTE que inserte un contenido que contravenga la normativa legal vigente asumirá la responsabilidad exclusiva de los perjuicios y consecuencias derivadas de la misma, eximiendo a VIPA de cualquier responsabilidad.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (6, N'5. POLITICA DE PROPIEDAD INTELECTUAL E INDUSTRIAL                                                                                                                                                                                                              ', N'VIPA se rige por las leyes peruanas y se encuentra protegido por la legislación nacional e internacional sobre propiedad intelectual e industrial. A menos que se indique lo contrario, todos los materiales, incluidos los videos (ediciones propias, no videos de REPORTANTES), imágenes, ilustraciones, diseños, iconos, fotografías, dibujos, u otros trabajos, así como también todo material escrito y otros materiales que aparecen en VIPA (colectivamente "Las artes") son derecho de propiedad, marca registrada, y/u otra propiedad intelectual controlada o licenciado por VIPA. Si el REPORTANTE modifica, enmienda, altera o cambia cualquier arte de alguna manera, el REPORTANTE conoce que tal modificación, enmienda, alteración o cambio es un trabajo derivado, y/o que VIPA es propietario de todos los derechos en, con y el trabajo derivado, modificación, enmienda, alteración o cambio. Los textos, diseños, imágenes, bases de datos, logos, estructura, marcas y demás elementos de este sitio propiedad de VIPA están protegidos por las leyes y los tratados internacionales sobre propiedad intelectual e industrial. Cualquier reproducción, transmisión, adaptación, traducción, modificación, comunicación al público, o cualquier otra explotación de todo o parte del contenido de este sitio titularidad de VIPA, efectuada de cualquier forma o por cualquier medio, electrónico, mecánico u otro, están estrictamente prohibidos salvo autorización previa por escrito de VIPA o terceros titulares. Cualquier infracción de estos derechos puede dar lugar a procedimientos extrajudiciales o judiciales civiles o penales que correspondan. La legitimidad de los derechos de propiedad intelectual o industrial correspondientes a los contenidos aportados por los REPORTANTES es de la exclusiva responsabilidad de los mismos. A los efectos de preservar los posibles derechos de propiedad intelectual, en el caso de que cualquier REPORTANTE o un tercero considere que se ha producido una violación de sus legítimos derechos por la introducción de un determinado contenido en el sitio web, deberá notificar dicha circunstancia, vía correo electrónico a: contacto@aplicativos.pe.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (7, N'6. RESTRICCIÓN DE LA GARANTÍA                                                                                                                                                                                                                                  ', N'El REPORTANTE acepta que el uso que haga el REPORTANTE de la app y página web de www.vipa.io correrá por cuenta única y exclusiva del REPORTANTE permitida por ley. VIPA, sus funcionarios, directores, empleados, y agentes restringen todas las garantías, expresas o implícitas, en conexión con la app y página web de VIPA y el uso que los REPORTANTES hagan de ella. VIPA no da garantías o hace representaciones acerca de la precisión o integridad del contenido de este sitio o el contenido de cualquier sitio ligado a este sitio y no asume responsabilidad por cualquier error, equivocación o imprecisión en el contenido, agravio personal o daño de propiedad, de cualquier naturaleza, que resulten del acceso del REPORTANTE y uso de VIPA, acceso no autorizado o uso de nuestros servidores seguros y/o cualquier y toda información personal y/o información almacenada aquí, interrupción o cese de transmisión desde la app VIPA, errores del sistema, virus, troyanos o aquellos que podrán ser transmitidos a o hacia nuestro sitio web por terceros, y/o errores u omisiones de cualquier contenido o por cualquier pérdida o daño incurrido como resultado del uso de cualquier contenido proporcionado, enviado por correo electrónico, transmitido, o de otra manera hecho disponible a través de VIPA. VIPA no garantiza, respalda, o asume responsabilidad alguna por cualquier producto o servicio publicado u ofrecido por terceros a través de VIPA o cualquier página con hipervínculo o página web destacada en cualquier banner u otra publicidad, y VIPA no será responsable, de ninguna manera, de monitorizar cualquier transacción entre el REPORTANTE y las terceras partes proveedoras de dichos productos o servicios. Al igual que con la compra de cualquier producto o servicio a través de cualquier medio o en cualquier ambiente, usted deberá usar su mejor juicio y ejercitar la precaución cuando fuese apropiado. Asimismo, VIPA no garantiza la disponibilidad, continuidad ni la infalibilidad del funcionamiento de la app y el sitio web, y en consecuencia, excluye, en la máxima medida permitida por la legislación vigente, cualquier responsabilidad por los daños y perjuicios de toda naturaleza que puedan deberse a la falta de disponibilidad o de continuidad del funcionamiento de la app y el sitio web y de los servicios habilitados en el mismo, así como a los errores en el acceso a las distintas páginas web o aquellas desde las que, en su caso, se presten dichos servicios. Por último, VIPA no se hará responsable por daños personales o daños/pérdidas materiales consecuencia de la utilización de dispositivos móviles en la vía pública y recomienda enfáticamente manipular con cuidado sus bienes personales y no exponerse a posibles agresiones durante la realización de un reporte.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (8, N'7. LIMITACIÓN DE LA RESPONSABILIDAD                                                                                                                                                                                                                            ', N'Bajo ningún evento VIPA, sus funcionarios, directores, empleados, o agentes, serán responsables ante usted por cualquier daño directo, indirecto, incidental, especial, punitivo o consecuente que resulte de cualquier error, equivocaciones, o imprecisiones de contenido, agravios personales o daños de la propiedad de cualquier naturaleza, resultado de su acceso a y uso de VIPA, acceso no autorizado a, o uso de nuestros servidores seguros y/o cualquier y toda información personal y/o información almacenada aquí, interrupción o cese de transmisión a, o desde la página web de www.vipa.io, errores del sistema, virus, troyanos o aquellos que podrían ser transmitidos a o hacia nuestro sitio web por terceros, y/o errores y omisiones de cualquier contenido o por cualquier pérdida o daño incurrido como resultado del uso de cualquier contenido proporcionado, enviado por correo electrónico, transmitido, o de otra manera hecho disponible a través de la página web, si está basado sobre una garantía, contrato, acto ilegal, o cualquier otra teoría legal, y si se le informa o no a la compañía sobre la posibilidad de dichos daños. Las limitaciones precedentes de responsabilidad se deberán aplicar a todo aquello exento permitido por ley en la jurisdicción aplicable. El REPORTANTE específicamente reconoce que VIPA no será responsable de cualquier contenido proporcionado por el REPORTANTE que sea difamatorio, ofensivo, o de una conducta ilegal de parte de terceros, asimismo no será responsable del riesgo de agravio o daño causado por todo lo precedente y que descansa enteramente sobre el REPORTANTE. En caso VIPA se encuentre obligado a revelar información del denunciante por parte de las autoridades competentes, no se podrá seguir protegiendo la anonimidad de EL REPORTANTE.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (9, N'8. POLITICA DE PROTECCION DE DATOS                                                                                                                                                                                                                             ', N'Al adquirir uno ó más de los servicios ofrecidos en VIPA solicitaremos como datos los siguientes: Nombres, apellidos completos y correo electrónico. VIPA requiere de esta información con el objetivo de mejorar la experiencia y garantizar la adecuada realización del servicio. VIPA podrá solicitar mayor información del REPORTANTE para fines estadísticos; el RERPORTANTE tendrá el derecho de aceptar compartirla si así lo desea. En general, no compartiremos la información personal de los REPORTANTES con terceros; pero habrán algunas excepciones y las mencionamos a continuación: Utilizaremos la información personal para propósitos internos, tales como mantener, evaluar, facilitar y mejorar las operaciones de VIPA. Su seguridad es nuestra principal preocupación. Derecho de información. Nuestra política de protección de datos regula el acceso y el uso de los servicios de la app VIPA y del sitio web www.vipa.io. VIPA cumple íntegramente con la legislación vigente en materia de protección de datos de carácter personal, y con los compromisos de confidencialidad propios de su actividad. Si el REPORTANTE decide registrarse en VIPA, se solicitarán los datos estrictamente necesarios para la consecución del fin al cual está destinada la app. A tales efectos se requerirá a los REPORTANTES el completar un formulario y la inclusión de sus datos personales. Su tratamiento estará enfocado únicamente a la consecución de estos fines, siempre dentro del marco normativo establecido. Finalidad: Los datos de los REPORTANTES registrados a través del formulario habilitado al efecto en VIPA son recabados, con las siguientes finalidades: Remitir comunicaciones electrónicas promocionales e informativas sobre este sector y otros sectores afines. Para participar en los procesos de premiación o sorteos que puede realizarse entre los REPORTANTES de VIPA. El REPORTANTE garantiza que los datos personales facilitados a VIPA son veraces y se hace responsable de comunicar cualquier modificación de los mismos. El REPORTANTE garantiza que toda la información de carácter personal que facilite es exacta y está actualizada de forma que responde con veracidad a la situación del REPORTANTE. Corresponde y es obligación del REPORTANTE mantener, en todo momento, sus datos actualizados, siendo el REPORTANTE el único responsable de la inexactitud o falsedad de los datos facilitados y de los perjuicios que pueda causar por ello a VIPA o a terceros con motivo de la utilización de los servicios ofrecidos por VIPA. Consentimiento del REPORTANTE. Al llenar el formulario y hacer clic para enviar los datos, el REPORTANTE manifiesta haber leído y aceptado expresamente la presente política de privacidad y las condiciones de uso, y otorga su consentimiento inequívoco y expreso al tratamiento de sus datos personales conforme a las finalidades informadas y servicios que presta VIPA. Asimismo, el REPORTANTE consiente que en el momento de registrarse, sus contenidos serán visibles públicamente para el resto de REPORTANTES en VIPA así como en los diversos buscadores de Internet. Comunicaciones electrónicas: El cumplimiento y envío del formulario electrónico de VIPA, supone el consentimiento expreso del REPORTANTE al envío de un boletín en el que se incluyan las noticias, novedades e información más relevantes del sitio web, así como las comunicaciones electrónicas relacionadas con el sector de servicios y otros sectores afines. VIPA a través de su correo electrónico, brinda la opción a los REPORTANTES que lo soliciten, puedan modificar o eliminar estos servicios de forma sencilla, rápida y gratuita. Cookies e IPs. El REPORTANTE acepta el uso de cookies y seguimientos de IPs. Nuestro analizador de tráfico del site utiliza cookies y seguimientos de IPs que nos permiten recoger datos para efectos estadísticos. No obstante, el REPORTANTE si lo desea puede desactivar y/o eliminar estas cookies siguiendo las instrucciones de su navegador de Internet. Este sitio web informa que podría estar adherido al sistema publicitario de Google que utiliza “cookies” para mostrar contenidos publicitarios relacionados con la navegación realizada por el REPORTANTE. Cuando un REPORTANTE accede a un sitio web adscrito al servicio de Google y hace clic en él se introduce una cookie en su navegador, a través de la cual Google recopila información de la navegación del REPORTANTE para gestionar y luego publicar anuncios a través del programa de publicidad Google. El REPORTANTE puede inhabilitar en cualquier momento el uso de la cookie de Google a través de la desactivación de la cookie en su navegador. VIPA no utiliza técnicas de "spamming" y únicamente tratará los datos que el REPORTANTE transmita mediante el formulario electrónico de la app o mensajes de correo electrónico. Derecho de Acceso, Rectificación y Cancelación de Datos. El REPORTANTE tiene derecho a acceder a esta información, a rectificarla si los datos son erróneos y a darse de baja de los servicios de VIPA. Estos derechos pueden hacerse efectivos mediante la propia configuración de la página web, o bien a través de nuestro correo electrónico: contacto@aplicativos.pe indicando el asunto de referencia. El tratamiento de los datos de carácter personal, así como el envío de comunicaciones comerciales realizadas por medios electrónicos, son conformes a la legislación peruana vigente y según lo especificado en el Reglamento de la Ley Nº 29733, Ley de Protección de Datos Personales; y en la Ley N° 30096, Ley de Delitos Informáticos. Medidas de Seguridad VIPA, cuenta con los procedimientos de seguridad y privacidad mínimos para el resguardo correcto de la información personal de los REPORTANTES. Modificación de la presente Política de Protección de Datos VIPA se reserva el derecho a modificar la presente política para adaptarla a futuras normas legislativas o jurisprudenciales.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (10, N'9. INDEMNIDAD                                                                                                                                                                                                                                                  ', N'El REPORTANTE acepta defender, indemnizar y mantener a VIPA, sus funcionarios, directores, empleados y agentes, libres de toda responsabilidad de y contra cualquier y todo reclamo, daño, obligación, pérdida, responsabilidad, costo o deuda, y gasto (incluido pero no limitado a los honorarios del abogado) provenientes del uso y acceso del REPORTANTE a VIPA; violación del REPORTANTE de cualquier término de los Términos de Servicio, Política de Privacidad de VIPA, Política de Protección de Datos de VIPA o Política de Propiedad Intelectual de VIPA; violación del REPORTANTE de cualquier derecho de terceras partes, sin incluir limitación alguna de derechos de propiedad, propiedad, o derechos de privacidad; cualquier reclamo que alguna de las contribuciones o Artes de sus REPORTANTES que cause daño a terceras partes. Esta defensa y obligación de indemnización sobrevivirá en los Términos de Servicio y en cualquier y todo uso de la página web de VIPA (www.vipa.io).')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (11, N'10. PROHIBICIÓN DE REVENDER EL SERVICIO                                                                                                                                                                                                                        ', N'El REPORTANTE está obligado a no reproducir, duplicar, copiar, vender, revender o explotar con cualquier propósito comercial, ninguna parte del Servicio, uso del Servicio, o acceso al Servicio.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (12, N'11. CAPACIDAD DE ACEPTACIÓN DE LOS TÉRMINOS DE SERVICIO                                                                                                                                                                                                        ', N'El REPORTANTE afirma que es mayor de 18 años de edad, o es un menor emancipado, o posee consentimiento legal parental o de su apoderado, y puede y es competente para aceptar los términos, condiciones, obligaciones, afirmaciones, representaciones, y garantías especificadas en los Términos de Servicio, y para acatar y cumplir con los Términos de Servicio. En todos los casos, el REPORTANTE afirma que es mayor de 18 años de edad, puesto que la app de VIPA no está dirigida a niños menores de 18 años. Si tienes menos de 18 años de edad, por favor no uses la app de VIPA. Conversa con tus padres acerca de qué apps podrías utilizar.')
INSERT [dbo].[terminocondicion] ([reglaid], [titulo], [descripcionregla]) VALUES (13, N'12. CESIÓN                                                                                                                                                                                                                                                     ', N'Los Términos de Servicio, y cualquier derecho y licencia otorgada en el presente, no pueden ser transferido o cedidos por el REPORTANTE, pero pueden ser cedidos por VIPA sin ninguna restricción.')
SET IDENTITY_INSERT [dbo].[terminocondicion] OFF
GO
SET IDENTITY_INSERT [dbo].[tiporeporte] ON 

INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (1, N'Vehículo sin conductor', N'1', 0)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (2, N'Bloquea Garaje', N'1', 1)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (3, N'Línea Amarilla/Zona Rigida', N'1', 1)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (4, N'Bloquea Hidratante', N'1', 1)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (5, N'Sobre Vereda o Áreas Verdes', N'1', 1)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (6, N'Discapacitados', N'1', 1)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (7, N'Sobre Ciclovía', N'1', 1)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (8, N'Vehículo con conductor', N'1', 0)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (9, N'En Contra del Tráfico', N'1', 8)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (10, N'Pasó Luz Roja', N'1', 8)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (11, N'Bloquea Intersección', N'1', 8)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (12, N'Pico y Placa', N'1', 8)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (13, N'Uso de Carril de Emergencia', N'1', 8)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (14, N'Sobre Paso Peatonal', N'1', 8)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (15, N'Sobre Ciclovía', N'1', 8)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (16, N'Conduce Sobre Vereda', N'1', 8)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (17, N'Transporte Público', N'1', 0)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (18, N'Taxi Colectivo Ilegal', N'1', 17)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (19, N'Faltas de Transporte Público', N'1', 17)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (20, N'Mal Uso del Paradero', N'1', 17)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (21, N'No Respetar la Ruta', N'1', 17)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (22, N'Covid-19', N'1', 0)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (23, N'No Usa Mascarilla', N'1', 22)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (24, N'No Respeta Toque de Queda', N'1', 22)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (25, N'Actividad Masiva', N'1', 22)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (26, N'No Respeta Aforo en Transporte Público', N'1', 22)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (27, N'Actividad Comercial No Permitida', N'1', 22)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (28, N'No Respeta Distanciamiento Social', N'1', 22)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (29, N'Violencia Pública y Derechos Humanos', N'1', 0)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (30, N'Abuso de Autoridad', N'1', 29)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (31, N'Arresto Injustificado', N'1', 29)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (32, N'Uso Desmedido de la Fuerza', N'1', 29)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (33, N'Basura, Desmonte, Ruidos y Otros', N'1', 0)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (34, N'Vehículo arroja residuos y/o desmonte en la calle', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (35, N'Comercios, restaurantes o viviendas que sacan residuos fuera', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (36, N'Ruidos Molestosos', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (37, N'Acoso Sexual', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (38, N'No Usa Cruce o Puente Peatonal', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (39, N'Tramitador Informal o Corrupción', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (40, N'Construcción o Comercio Sin Licencia', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (41, N'Venta Ilegal de Pirotécnicos', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (42, N'Agresión de Ciudadano o Autoridad', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (43, N'Bebidas Alcohólicos en Vía Pública', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (44, N'Apropiación Ilegal del Espacio Público', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (45, N'Apropiación Ilegal del Espacio Público', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (46, N'Expendio de licor fuera de hora', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (47, N'Robo / Asalto', N'1', 33)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (48, N'Pistas, Veredas y Señalización', N'1', 0)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (49, N'Semáforo Malogrado', N'1', 48)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (50, N'Pista o Vereda en Mal Estado', N'1', 48)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (51, N'Jardín o Parque en Mal Estado', N'1', 48)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (52, N'Falta Señalización', N'1', 48)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (53, N'Mascotas', N'1', 0)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (54, N'Encontrado', N'1', 48)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (55, N'Maltrato Animal', N'1', 48)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (56, N'No Recoge Excretas', N'1', 48)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (57, N'Limpiado de fachada Lima', N'1', 0)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (58, N'Antes', N'1', 57)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (59, N'Después', N'1', 57)
INSERT [dbo].[tiporeporte] ([idtiporeporte], [reportenombre], [reporteindicador], [idreportepadre]) VALUES (60, N'Actos Vandálicos', N'1', 29)
SET IDENTITY_INSERT [dbo].[tiporeporte] OFF
GO
INSERT [dbo].[usuarioscliente] ([idusuariocliente], [idcliente], [nombreusuariocliente], [correousuariocliente], [contraseñausuariocliente]) VALUES (N'CU000001', N'CE000001', N'Elias Manrique', N'emanrique@mlima.pe', N'123')
INSERT [dbo].[usuarioscliente] ([idusuariocliente], [idcliente], [nombreusuariocliente], [correousuariocliente], [contraseñausuariocliente]) VALUES (N'CU000002', N'CE000001', N'Alejandro Toledo', N'etoledo@mlima.pe', N'123')
INSERT [dbo].[usuarioscliente] ([idusuariocliente], [idcliente], [nombreusuariocliente], [correousuariocliente], [contraseñausuariocliente]) VALUES (N'CU000003', N'CE000002', N'Elian Oliva', N'eoliva@mlavictoria.pe', N'123')
INSERT [dbo].[usuarioscliente] ([idusuariocliente], [idcliente], [nombreusuariocliente], [correousuariocliente], [contraseñausuariocliente]) VALUES (N'CU000004', N'CE000002', N'Maricielo Manrique', N'mmanrique@mlavictoria.pe', N'123')
GO
INSERT [dbo].[valoracionreporte] ([idvaloracion], [segundotipovaloracion], [idreporte], [idreportador], [idtiporeporte]) VALUES (N'VR000001', N'grave', N'IN000001', N'RE000001', 2)
INSERT [dbo].[valoracionreporte] ([idvaloracion], [segundotipovaloracion], [idreporte], [idreportador], [idtiporeporte]) VALUES (N'VR000002', N'grave', N'IN000002', N'RE000002', 10)
GO
ALTER TABLE [dbo].[cliente] ADD  DEFAULT (NULL) FOR [nombrecliente]
GO
ALTER TABLE [dbo].[cliente] ADD  DEFAULT (NULL) FOR [nombredistrito]
GO
ALTER TABLE [dbo].[cliente] ADD  DEFAULT (NULL) FOR [fechacreacion]
GO
ALTER TABLE [dbo].[cuentaverificada] ADD  DEFAULT (NULL) FOR [tokenverificacion]
GO
ALTER TABLE [dbo].[cuentaverificada] ADD  DEFAULT (NULL) FOR [estadoperfil]
GO
ALTER TABLE [dbo].[cuentaverificada] ADD  DEFAULT (NULL) FOR [fechacreacion]
GO
ALTER TABLE [dbo].[estadoperfilranking] ADD  DEFAULT (NULL) FOR [puntosactuales]
GO
ALTER TABLE [dbo].[estadoperfilranking] ADD  DEFAULT (NULL) FOR [posicionglobalranking]
GO
ALTER TABLE [dbo].[notificacion] ADD  DEFAULT (NULL) FOR [nombrenotificacion]
GO
ALTER TABLE [dbo].[notificacion] ADD  DEFAULT (NULL) FOR [descripcionnotificacion]
GO
ALTER TABLE [dbo].[notificacion] ADD  DEFAULT (NULL) FOR [urlincidente]
GO
ALTER TABLE [dbo].[notificacion] ADD  DEFAULT (NULL) FOR [repeticionnotificacion]
GO
ALTER TABLE [dbo].[notificacion] ADD  DEFAULT (NULL) FOR [fechadespliegue]
GO
ALTER TABLE [dbo].[preguntafrecuente] ADD  CONSTRAINT [DF__preguntas__descr__5BE2A6F2]  DEFAULT (NULL) FOR [descripcionpregunta]
GO
ALTER TABLE [dbo].[preguntafrecuente] ADD  CONSTRAINT [DF__preguntas__descr__5CD6CB2B]  DEFAULT (NULL) FOR [descripcionrespuesta]
GO
ALTER TABLE [dbo].[rankingglobal] ADD  DEFAULT (NULL) FOR [nombrenivelranking]
GO
ALTER TABLE [dbo].[rankingglobal] ADD  DEFAULT (NULL) FOR [descripcionnivelranking]
GO
ALTER TABLE [dbo].[rankingglobal] ADD  DEFAULT (NULL) FOR [puntosmaximos]
GO
ALTER TABLE [dbo].[reportador] ADD  DEFAULT (NULL) FOR [aliasusuario]
GO
ALTER TABLE [dbo].[reportador] ADD  DEFAULT (NULL) FOR [perfilvisible]
GO
ALTER TABLE [dbo].[reportador] ADD  DEFAULT (NULL) FOR [nombrereportador]
GO
ALTER TABLE [dbo].[reportador] ADD  DEFAULT (NULL) FOR [apellidosreportador]
GO
ALTER TABLE [dbo].[reportador] ADD  DEFAULT (NULL) FOR [correoreportador]
GO
ALTER TABLE [dbo].[reportador] ADD  DEFAULT (NULL) FOR [contraseñareportador]
GO
ALTER TABLE [dbo].[reporte] ADD  CONSTRAINT [DF__reporte__idurlvi__6754599E]  DEFAULT (NULL) FOR [idurlvideo]
GO
ALTER TABLE [dbo].[reporte] ADD  CONSTRAINT [DF__reporte__idurlgp__68487DD7]  DEFAULT (NULL) FOR [idurlgps]
GO
ALTER TABLE [dbo].[reporte] ADD  CONSTRAINT [DF__reporte__nombred__693CA210]  DEFAULT (NULL) FOR [nombredistrito]
GO
ALTER TABLE [dbo].[reporte] ADD  CONSTRAINT [DF__reporte__idplaca__6A30C649]  DEFAULT (NULL) FOR [idplacavehiculo]
GO
ALTER TABLE [dbo].[reporte] ADD  CONSTRAINT [DF__reporte__descrip__6B24EA82]  DEFAULT (NULL) FOR [descripcionreporte]
GO
ALTER TABLE [dbo].[reporte] ADD  CONSTRAINT [DF__reporte__fechacr__6C190EBB]  DEFAULT (NULL) FOR [fechacreacion]
GO
ALTER TABLE [dbo].[terminocondicion] ADD  CONSTRAINT [DF__terminosc__descr__6E01572D]  DEFAULT (NULL) FOR [descripcionregla]
GO
ALTER TABLE [dbo].[tiporeporte] ADD  CONSTRAINT [DF__tiporepor__repor__6EF57B66]  DEFAULT (NULL) FOR [reportenombre]
GO
ALTER TABLE [dbo].[tiporeporte] ADD  CONSTRAINT [DF__tiporepor__repor__6FE99F9F]  DEFAULT (NULL) FOR [reporteindicador]
GO
ALTER TABLE [dbo].[usuarioscliente] ADD  DEFAULT (NULL) FOR [nombreusuariocliente]
GO
ALTER TABLE [dbo].[usuarioscliente] ADD  DEFAULT (NULL) FOR [correousuariocliente]
GO
ALTER TABLE [dbo].[usuarioscliente] ADD  DEFAULT (NULL) FOR [contraseñausuariocliente]
GO
ALTER TABLE [dbo].[valoracionreporte] ADD  DEFAULT (NULL) FOR [segundotipovaloracion]
GO
ALTER TABLE [dbo].[estadoperfilranking]  WITH CHECK ADD  CONSTRAINT [estadoperfilranking$fk_estadoperfilranking_rankingglobal1_idx] FOREIGN KEY([nivelraking])
REFERENCES [dbo].[rankingglobal] ([nivelraking])
GO
ALTER TABLE [dbo].[estadoperfilranking] CHECK CONSTRAINT [estadoperfilranking$fk_estadoperfilranking_rankingglobal1_idx]
GO
ALTER TABLE [dbo].[reportador]  WITH CHECK ADD  CONSTRAINT [reportador$fk_reportador_cuentaverificada1_idx] FOREIGN KEY([verificacionperfil])
REFERENCES [dbo].[cuentaverificada] ([verificacionperfil])
GO
ALTER TABLE [dbo].[reportador] CHECK CONSTRAINT [reportador$fk_reportador_cuentaverificada1_idx]
GO
ALTER TABLE [dbo].[reporte]  WITH CHECK ADD  CONSTRAINT [reporte$fk_reporte_t_all_general_kind_reports1_idx] FOREIGN KEY([idtiporeporte])
REFERENCES [dbo].[tiporeporte] ([idtiporeporte])
GO
ALTER TABLE [dbo].[reporte] CHECK CONSTRAINT [reporte$fk_reporte_t_all_general_kind_reports1_idx]
GO
ALTER TABLE [dbo].[usuarioscliente]  WITH CHECK ADD  CONSTRAINT [usuarioscliente$fk_usuarioscliente_cliente1_idx] FOREIGN KEY([idcliente])
REFERENCES [dbo].[cliente] ([idcliente])
GO
ALTER TABLE [dbo].[usuarioscliente] CHECK CONSTRAINT [usuarioscliente$fk_usuarioscliente_cliente1_idx]
GO
USE [master]
GO
ALTER DATABASE [vipa_database] SET  READ_WRITE 
GO
